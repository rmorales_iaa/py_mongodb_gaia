# accessing to mongodb from phyton
# dependencies:
#   pip3 install pymongo
# ------------------------------------------------------------------------------
from pymongo.mongo_client import MongoClient
import csv
import logger
import logging
import os
# ------------------------------------------------------------------------------
# connection strings
url        = "mongodb://test:testIAA@161.111.167.194:18028/?compressors=zlib"
database   = "GAIA_DR_3"
collection = "gaia_source"
# ------------------------------------------------------------------------------
csv_file_name = ""
# ------------------------------------------------------------------------------
# init output (csv directory ad filename)
def output_init():
    out_path = os.getcwd() + "/csv/"

    # create the logger directory if it does not exist
    if (not os.path.isdir(out_path)):
        try:
            os.mkdir(out_path)
        except OSError:
            print("Creation of the directory %s failed" % out_path)
            return False
    global csv_file_name
    csv_file_name = out_path + logger.log_file_name + ".csv"
    logging.warning(f"Using for output csv: '{csv_file_name}'")
# ------------------------------------------------------------------------------
def create_csv(csv_header, mongo_cursor, divider = ","):
    # open the file in the write mode
    f = open(csv_file_name, 'w')

    # create the csv writer
    writer = csv.writer(f)

    # write the header
    writer.writerow(csv_header)

    # write the row sequence
    for x in mongo_cursor:
        row = []
        for s in str(x).split(","):
            item = (s[s.find(": ")+1:]) \
                 .replace("}", "") \
                 .strip()
            row.append(item)
        writer.writerow(row)

    # close the file
    f.close()
# ------------------------------------------------------------------------------
def query_mongo():

    # query parameters
    min_ra =  115.85413400691159  # minRa  in decimal degree
    max_ra =  115.89413400691159  # maxRa  in decimal degree
    min_dec = 40.39653039855756   # minDec in decimal degree
    max_dec = 40.43653039855756   # max dec in decimal degree

    # open the connection
    mongo_client = MongoClient(url)
    mongo_db = mongo_client[database]
    mongo_col = mongo_db[collection]

    # query
    query = {
        "ra": {"$gte": min_ra,  "$lte": max_ra}
     , "dec": {"$gte": min_dec, "$lte": max_dec}
    }

    # projection
    projection = {
        "ra": 1
      , "dec": 1
    }

    csv_header = ['_id', 'ra', 'dec']

    # sort
    sort = [
        ("ra", 1)
        , ("dec", -1)
    ]

    # execute query
    logging.info("Querying mongo")
    mongo_cursor = mongo_col.find(query, projection, sort=sort)

    result_count = len(list(mongo_cursor))
    mongo_cursor.rewind()

    logging.info(f"Result count: {result_count}")

    # create the csv file
    create_csv(csv_header, mongo_cursor)

    # close the connection
    mongo_client.close()
# ------------------------------------------------------------------------------
if __name__ == '__main__':
    if (logger.logger_init()) :
        output_init()
        logging.info(f"Mongo query starts")
        query_mongo()
        logging.info(f"Mongo query ends")
# ------------------------------------------------------------------------------
